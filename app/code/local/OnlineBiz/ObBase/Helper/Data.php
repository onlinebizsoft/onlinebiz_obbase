<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://store.onlinebizsoft.com/license.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@onlinebizsoft.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     OnlineBiz <sales@onlinebizsoft.com>
 * @copyright  2007-2011 OnlineBiz
 * @license    http://store.onlinebizsoft.com/license.txt
 * @version    1.0.1
 * @link       http://store.onlinebizsoft.com
 */

class OnlineBiz_ObBase_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_own_module_name = false;
	
	public function getOwnModuleName() {
		if (!$this->_own_module_name) {
			$this->_own_module_name = substr(get_class($this), 10, strpos(get_class($this), '_', 10)-10);//auto extract the own module name from helper class
		}
		return $this->_own_module_name;
	}
	
	public function isEnabled() {
		return $this->_isActivated('OnlineBiz_'.$this->getOwnModuleName());
	}
	
	//for writing trace or important information to log if debug is enabled
	public function debug($string) {
		if (Mage::getStoreConfig(strtolower($this->getOwnModuleName()). '/general/debug')) {
			Mage::log($this->getOwnModuleName() . ' : ' . $string, null, 'obdebug.log');
		}
	}
	
	//only call when a fatal error happen - should be written with all exception messages
	public function error($string) {
		Mage::log($this->getOwnModuleName() . ' : ' . $string, null, 'obdebug.log');	
	}

	public function isConflict() {
		$module = 'OnlineBiz_'.$this->getOwnModuleName();

		$rewrites = $this->getAllRewrites();

		$i = 0;
		foreach($rewrites as $key => $value)
		{
			$i++;
			
			$found = false;
			foreach ($value as $class_name) {
				if ((strpos($class_name, $module) === 0)) {
					$found = true;
					break;
				}
			}
			
			$t = explode('/', $key);
			$moduleName = $t[0];
			$className = $t[1];
			$rewriteClasses = join(', ', $value);
			$conflict = 0;
			if (count($value) > 1)
				$conflict = 1;
			if ($found && $conflict) {
				return true;
			}
		}
		return false;
	}
	
	protected function _isActivated($module, $key = false, $generalConfig = false)
	{
		//no need to pass Enabled path
		if (!$generalConfig) {
			$generalConfig = strtolower($this->getOwnModuleName()). '/general/enabled';
		}
		
		//normalize domain
		$servStr = $_SERVER['HTTP_HOST'];
		$servStr = str_replace('https://', '', $servStr);
		$servStr = str_replace('http://', '', $servStr);
		$servStr = str_replace('www.', '', $servStr);		
		
		$keys = preg_split("/(\r\n|\n|\r)/", Mage::getStoreConfig('managekey/general/keys'));
		foreach($keys as $key){
			if(base64_encode(md5($servStr.$module)) == $key && Mage::getStoreConfig($generalConfig)) {
				return true;
			}
		}		
		
		//test/dev/local sites
		if((preg_match('/dev./',$servStr) || preg_match('/test./',$servStr) || preg_match('/demo./',$servStr)) && Mage::getStoreConfig($generalConfig))
			return true;
			
		if(($servStr == '127.0.0.1' || preg_match('/localhost/',$servStr)) && Mage::getStoreConfig($generalConfig))
			return true;
		
		//pass module key when check (old key management system)
		if($key) {
			if(base64_encode(md5($servStr.$module)) == $key && Mage::getStoreConfig($generalConfig))
			return true;
		}
		
		return false;
	}	
	/**
	 * create an array with all config.xml files
	 *
	 */
	protected function _getConfigFilesList()
	{
		$retour = array();
		//$codePath = Mage::getStoreConfig('system/filesystem/code');
		$codePath = BP . DS . 'app' . DS . 'code';
		
		$locations = array();
		$locations[] = $codePath.'/local/';
		$locations[] = $codePath.'/community/';
		
		foreach ($locations as $location)
		{
			//parse every sub folders (means extension folders)
			$poolDir = opendir($location);
			while($namespaceName = readdir($poolDir))
			{
				if (!$this->directoryIsValid($namespaceName))
					continue;
					
				//parse modules within namespace
				$namespacePath = $location.$namespaceName.'/';
				$namespaceDir = opendir($namespacePath);
				while($moduleName = readdir($namespaceDir))
				{
					if (!$this->directoryIsValid($moduleName))
						continue;
					
					if (!Mage::helper('core')->isModuleEnabled($namespaceName.'_'.$moduleName))
						continue;
					
					$modulePath = $namespacePath.$moduleName.'/';
					$configXmlPath = $modulePath.'etc/config.xml';
					
					if (file_exists($configXmlPath))
						$retour[] = $configXmlPath;
				}
				closedir($namespaceDir);
			}
			closedir($poolDir);
		}
		
		return $retour;
	}
	/**
	 * 
	 *
	 * @param unknown_type $dirName
	 * @return unknown
	 */
	private function directoryIsValid($dirName)
	{
		switch ($dirName) {
			case '.':
			case '..':
			case '':
				return false;
				break;		
			default:
				return true;
				break;
		}
	}
	private function manageModule($moduleName)
	{
		switch ($moduleName) {
			case 'global':
				return false;
				break;		
			default:
				return true;
				break;
		}		
	}
	
	/**
	 * Return all rewrites for a config.xml
	 *
	 * @param unknown_type $configFilePath
	 */
	public function getRewriteForFile($configFilePath, $results)
	{
        try {
            //load xml
            $xmlcontent = file_get_contents($configFilePath);
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xmlcontent);

            //parse every node types
            $nodeTypes = array('blocks', 'models', 'helpers');
            foreach ($nodeTypes as $nodeType) {
                if (!$domDocument->documentElement)
                    continue;

                foreach ($domDocument->documentElement->getElementsByTagName($nodeType) as $nodeTypeMarkup) {
                    foreach ($nodeTypeMarkup->getElementsByTagName('rewrite') as $markup) {
                        //parse child nodes
                        $moduleName = $markup->parentNode->tagName;
                        if ($this->manageModule($moduleName)) {
                            foreach ($markup->getElementsByTagName('*') as $childNode) {
                                //get information
                                $className = $nodeType . '_' . $childNode->tagName;
                                $rewriteClass = $childNode->nodeValue;

                                //add to result
                                $key = $moduleName . '/' . $className;
                                if (!isset($results[$key]))
                                    $results[$key] = array();
                                $results[$key][] = $rewriteClass;
                            }
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            return $results;
        }

        return $results;
	}
	
	public function getAllRewrites() {
	
		$xml_list = $this->_getConfigFilesList();
	
		$rewrites = array();
		foreach($xml_list as $configFile)
		{	
			$rewrites = $this->getRewriteForFile($configFile, $rewrites);
		}
		
		return $rewrites;

	}
	

	
}


