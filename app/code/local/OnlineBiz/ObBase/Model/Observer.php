<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://store.onlinebizsoft.com/license.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@onlinebizsoft.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     OnlineBiz <sales@onlinebizsoft.com>
 * @copyright  2007-2011 OnlineBiz
 * @license    http://store.onlinebizsoft.com/license.txt
 * @version    1.0.1
 * @link       http://store.onlinebizsoft.com
 */

class OnlineBiz_ObBase_Model_Observer extends Mage_Core_Model_Abstract
{
	public function update(){
		if ((!(Mage::app()->loadCache('obbase_update_last_check')) || (time()-Mage::app()->loadCache('obbase_update_last_check')) > 15*60) //avoid continous check when remote server is down
			&& (!(Mage::app()->loadCache('obbase_update_last_success')) || (time()-Mage::app()->loadCache('obbase_update_last_check')) > 24*60*60)) //only keep checking when last success is more than 24 hours
		{
			Mage::app()->saveCache(time(), 'obbase_update_last_check');//saving last check time first
			//verify license key status
			$this->verifyLicenseKeys();
			//send extension information
			$this->reportExtensionInformation();
			//update extension feed
			$this->updateExtensionFeed();
			//update important news
			$this->updateNews();
		}
	}
	
	protected function verifyLicenseKeys() {
	}
	protected function reportExtensionInformation() {
	}
	protected function updateExtensionFeed() {
		$this->setFeedUrl(OnlineBiz_ObBase_Helper_Config::EXTENSIONS_FEED_URL);
		
		$exts = array();
		try{
			$Node = $this->getFeedData();
			if(!$Node) return false;
			foreach($Node->children() as $ext){
				$exts[(string)$ext->name] = array(
					'display_name' => (string)$ext->display_name,
					'version' => (string)$ext->version,
					'url'		=> (string)$ext->url
				);
			}

			Mage::app()->saveCache(serialize($exts), 'obbase_extensions_feed');
			Mage::app()->saveCache(time(), 'obbase_extensions_feed_lastcheck');
			return true;
		}catch(Exception $E){
			return false;
		}
	}
	protected function updateNews() {
		$this->setFeedUrl(OnlineBiz_ObBase_Helper_Config::UPDATES_FEED_URL);
		
		$feedData = array();
		try{
			$Node = $this->getFeedData();
			if(!$Node) return false;
			foreach($Node->children() as $item){
				
				 if($this->isInteresting($item)){
				 	$date = strtotime((string)$item->date);
					if(!Mage::getStoreConfig('obbase/install/run') || (Mage::getStoreConfig('obbase/install/run') < $date)){
					 	$feedData[] = array(
	                 	   'severity'      => (int)$item->severity ? (int)$item->severity : 3,
	                 	   'date_added'    => $this->getDate((string)$item->date),
	                 	   'title'         => (string)$item->title,
	                 	   'description'   => (string)$item->content,
	                 	   'url'           => (string)$item->url,
	                	);
					}
				 }
			}
			if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(($feedData));
            }

			Mage::app()->saveCache(time(), 'obbase_updates_feed_lastcheck');
			return true;
		}catch(Exception $E){
			return false;			
		}
	}
	/**
     * Retrieve feed data as XML element
     *
     * @return SimpleXMLElement
     */
    public function getFeedData()
    {
        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
            'timeout'   => 5
        ));
        $curl->write(Zend_Http_Client::GET, $this->getFeedUrl(), '1.0');
        $data = $curl->read();
        if ($data === false) {
			
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $data = trim($data[1]);
        $curl->close();

        try {
            $xml  = new SimpleXMLElement($data);
        }
        catch (Exception $e) {
            return false;
        }
        return $xml;
    }
	
	
    /**
     * Retrieve DB date from RSS date
     *
     * @param string $rssDate
     * @return string YYYY-MM-DD YY:HH:SS
     */
    public function getDate($rssDate)
    {
        return gmdate('Y-m-d H:i:s', strtotime($rssDate));
    }

}
